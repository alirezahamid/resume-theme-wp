<!DOCTYPE html>
<html lang="en" class="crt crt-nav-on crt-nav-type1 crt-main-nav-on crt-sidebar-on crt-layers-3">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <?php wp_head() ?>
</head>

<body class="">
<div class="crt-wrapper">
    <header id="crt-header">
        <div class="crt-head-inner crt-container">
            <div class="crt-container-sm">
                <div class="crt-head-row">
                    <div id="crt-head-col1" class="crt-head-col text-left">
                        <a id="crt-logo" class="crt-logo" href="index.html">
                            <span><?php bloginfo('name'); ?></span>
                        </a>
                    </div>

                    <div id="crt-head-col2" class="crt-head-col text-right">
                        <div class="crt-nav-container crt-container hidden-sm hidden-xs">
                            <nav id="crt-main-nav">
                                <?php wp_nav_menu(array(
                                    'theme_location'=> 'headerMenuLocation',
                                    'container'       => ' ',
                                    'container_class' => 'clear-list',

                                )) ?>
                            </nav>
                        </div>
                    </div>

                   <!-- <div id="crt-head-col3" class="crt-head-col text-right">
                        <button id="crt-sidebar-btn" class="btn btn-icon btn-shade">
                            <span class="crt-icon crt-icon-side-bar-icon"></span>
                        </button>
                    </div>-->
                </div>
            </div><!-- .crt-head-inner -->
        </div>
        <?php
        $homePageBio = new WP_Query(array(
            'posts_per_page' => 1,
            'post_type' => 'bio'
        ));
        while ($homePageBio->have_posts()){
        $homePageBio->the_post();

        ?>
        <nav id="crt-nav-sm" class="crt-nav hidden-lg hidden-md">
            <ul class="clear-list">
                <li>
                    <a href="index.html" data-tooltip="Home"><img class="avatar avatar-42" src="<?php the_post_thumbnail_url('smallAvatar'); ?>" srcset="<?php the_post_thumbnail_url('smallAvatar'); ?>" alt=""></a>
                </li>
                <li>
                    <a href="experience.html" data-tooltip="Experience"><span class="crt-icon crt-icon-experience"></span></a>
                </li>
                <li>
                    <a href="portfolio.html" data-tooltip="Portfolio"><span class="crt-icon crt-icon-portfolio"></span></a>
                </li>
                <li>
                    <a href="testimonials.html" data-tooltip="References"><span class="crt-icon crt-icon-references"></span></a>
                </li>
                <li>
                    <a href="contact.html" data-tooltip="Contact"><span class="crt-icon crt-icon-contact"></span></a>
                </li>
                <li>
                    <a href="category.html" data-tooltip="Blog"><span class="crt-icon crt-icon-blog"></span></a>
                </li>
            </ul>
        </nav><!-- #crt-nav-sm -->
        <?php } wp_reset_postdata() ?>
    </header><!-- #crt-header -->