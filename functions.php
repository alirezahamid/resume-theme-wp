<?php
function resume_files(){
    wp_enqueue_style('resume_main_style',get_stylesheet_uri(),NULL, microtime());
    wp_enqueue_script('modernizr_script', get_template_directory_uri() .'/assets/js/vendor/modernizr-3.3.1.min.js');
    wp_enqueue_script('jquery_script', get_template_directory_uri() .'/assets/js/vendor/jquery-1.12.4.min.js');
    wp_enqueue_script('plugin_script', get_template_directory_uri() .'/assets/js/plugins.js');
    wp_enqueue_script('main_script', get_template_directory_uri() .'/assets/js/scripts.js',array('jquery_script'));

}
add_action('wp_enqueue_scripts','resume_files');

function resume_features(){
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    add_image_size('largeAvatar', 192 , 192 , true);
    add_image_size('smallAvatar', 42 , 42 , true);
    register_nav_menu('headerMenuLocation','Header Menu Location');
}
add_action('after_setup_theme','resume_features');


// Post types
function resume_post_type (){
    // Event Post Type
    register_post_type('bio',array(
        'rewrite' => array('slug'=>'bios') ,
        'has_archive' => true,
        'public' => true,
        /*        'show_in_rest' => true,*/
        'supports' => array('title','excerpt','editor','thumbnail'),
        'labels' => array(
            'name' => 'Bio',
            'add_new_item' => 'Add New Bio',
            'edit_item' => 'Edit Bio',
            'all_items' => 'All Bios',
            'singular_name' => 'Bio'
        ),
        'menu_icon' => 'dashicons-universal-access'
    ));

}
add_action('init','resume_post_type');