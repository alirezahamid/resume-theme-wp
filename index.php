<?php get_header() ?>
<div id="crt-container" class="crt-container">
    <?php
    $homePageBio = new WP_Query(array(
        'posts_per_page' => 1,
        'post_type' => 'bio'
    ));
    while ($homePageBio->have_posts()){
    $homePageBio->the_post();

    ?>
    <div id="crt-nav-wrap" class="hidden-sm hidden-xs">
        <div id="crt-nav-inner">
            <div class="crt-nav-cont">
                <div id="crt-nav-scroll">
                    <nav id="crt-nav" class="crt-nav">
                        <ul class="clear-list">
                            <li>
                                <a href="index.html" data-tooltip="Home"><img class="avatar avatar-42" src="<?php the_post_thumbnail_url('smallAvatar'); ?>" srcset="<?php the_post_thumbnail_url('smallAvatar'); ?>" alt=""></a>
                            </li>
                            <li>
                                <a href="experience.html" data-tooltip="Experience"><span class="crt-icon crt-icon-experience"></span></a>
                            </li>
                            <li>
                                <a href="portfolio.html" data-tooltip="Portfolio"><span class="crt-icon crt-icon-portfolio"></span></a>
                            </li>
                            <li>
                                <a href="testimonials.html" data-tooltip="References"><span class="crt-icon crt-icon-references"></span></a>
                            </li>
                            <li>
                                <a href="contact.html" data-tooltip="Contact"><span class="crt-icon crt-icon-contact"></span></a>
                            </li>
                            <li>
                                <a href="category.html" data-tooltip="Blog"><span class="crt-icon crt-icon-blog"></span></a>
                            </li>
                        </ul>
                    </nav>
                </div>

                <div id="crt-nav-tools" class="hidden">
                    <span class="crt-icon crt-icon-dots-three-horizontal"></span>

                    <button id="crt-nav-arrow" class="clear-btn">
                        <span class="crt-icon crt-icon-chevron-thin-down"></span>
                    </button>
                </div>
            </div>
            <div class="crt-nav-btm"></div>
        </div>
    </div><!-- .crt-nav-wrap -->

    <div class="crt-container-sm">
        <div class="crt-paper-layers">
            <div class="crt-paper clear-mrg">


                <section class="section">

                    <div class="crt-card crt-card-wide bg-primary text-center">
                        <div class="crt-card-avatar">
                            <img class="avatar avatar-195" src="<?php the_post_thumbnail_url('largeAvatar'); ?>" srcset="<?php the_post_thumbnail_url('largeAvatar'); ?>" width="195" height="195" alt="">
                        </div>
                        <div class="crt-card-info">

                            <h2 class="text-upper"><?php the_title() ?></h2>

                            <p class="text-muted"><?php echo get_the_excerpt(); ?></p>
                            <ul class="crt-social clear-list">
                                <?php the_field('bio_social'); ?>
                            </ul>
                        </div>
                    </div>
                </section>
                <!-- .section -->

                <section class="section brd-btm padd-box">
                    <div class="row">
                        <div class="col-sm-12 clear-mrg text-box">
                            <h2 class="title-lg text-upper">About Me</h2>

                            <?php the_content(); ?>

                            <div class="row">
                                <div class="col-sm-9">
                                    <div class="crt-share-box clearfix">
                                        <button id="btn-share" class="btn btn-share btn-upper"><span class="crt-icon crt-icon-share-alt"></span>Share
                                        </button>
                                    </div>
                                    <!-- .crt-share -->
                                </div>
                                <div class="col-sm-3 text-right">
                                    <img src="<?php echo get_template_directory_uri().'/assets/images/uploads/signature.svg' ?>" alt="signature">
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
                <!-- .section -->

                <section class="section brd-btm padd-box">
                    <div class="row">
                        <div class="col-sm-6 clear-mrg">
                            <h2 class="title-thin text-muted">Personal Information</h2>
                            <?php the_field('personal_info'); ?>
                        </div>
                        <!-- .col-sm-6 -->

                        <div class="col-sm-6 clear-mrg">
                            <h2 class="title-thin text-muted">Languages</h2>

                            <?php the_field('language'); ?>

                        </div>
                        <!-- .col-sm-6 -->
                    </div>
                    <!-- .row -->
                </section>

                <!-- .section -->

                <section class="section brd-btm padd-box">
                    <div class="row">
                        <?php the_field('main_skills'); ?>
                    </div>
                    <!-- .row -->
                </section>
                <!-- .section -->

                <section class="section brd-btm padd-box">
                    <div class="row">
                        <div class="col-sm-6 clear-mrg">
                            <h2 class="title-thin text-muted">Other Skills</h2>
                            <?php the_field('other_skills'); ?>
                        </div>
                        <!-- .col-sm-6 -->
                        <div class="col-sm-6 clear-mrg">
                            <h2 class="title-thin text-muted">Interests</h2>

                            <ul class="styled-list clear-mrg">
                                <?php the_field('interests'); ?>
                            </ul>

                        </div>
                        <!-- .col-sm-6 -->
                    </div>
                    <!-- .row -->
                </section>
                <!-- .section -->
                <?php } wp_reset_postdata() ?>
                <!-- .section -->

<!--                <section class="section padd-box">
                    <h2 class="title-thin text-muted">Awards and Achievements</h2>

                    <div class="row">
                        <div class="col-sm-6 clear-mrg">
                            <div class="award-box">
                                <figure class="award-img">
                                    <img src="<?php /*echo get_template_directory_uri().'/assets/images/uploads/awards/award-01.png' */?>" alt="">
                                </figure>

                                <h3 class="award-title">Lake Marcelview</h3>

                                <div class="award-text text-muted clear-mrg">
                                    <p>Your brand is the core of your marketing, the central theme around your products and
                                        services.
                                        Your brand is not your Logo or your Company Name</p>
                                </div>
                            </div>
                        </div>


                        <div class="col-sm-6 clear-mrg">
                            <div class="award-box">
                                <figure class="award-img">
                                    <img src="<?php /*echo get_template_directory_uri().'/assets/images/uploads/awards/award-02.png' */?>" alt="">
                                </figure>

                                <h3 class="award-title">Lake Marcel Awards</h3>

                                <div class="award-text text-muted clear-mrg">
                                    <p>Your brand is the core of your marketing, the central theme around your products and
                                        services.</p>
                                </div>
                            </div>
                        </div>

                    </div>

                </section>  -->
                <!-- .section -->

            </div>
            <!-- .crt-paper -->
        </div>
        <!-- .crt-paper-layers -->
    </div>
    <!-- .crt-container-sm -->
</div>
<!-- .crt-container -->

<div id="crt-sidebar">
    <button id="crt-sidebar-close" class="btn btn-icon btn-light btn-shade">
        <span class="crt-icon crt-icon-close"></span>
    </button>

    <div id="crt-sidebar-inner">
        <nav id="crt-main-nav-sm" class="visible-xs text-center">

            <ul class="clear-list">
                <li><a href="index.html">home</a></li>
                <li><a href="portfolio.html">portfolio</a></li>
                <li class="has-sub-menu"><a href="#">pages</a>
                    <ul class="sub-menu">
                        <li><a href="typography.html">typography</a></li>
                        <li><a href="components.html">components</a></li>
                        <li><a href="search.html">search</a></li>
                        <li><a href="404.html">404</a></li>
                    </ul>
                </li>
                <li class="has-sub-menu"><a href="category.html">blog</a>
                    <ul class="sub-menu">
                        <li><a href="single.html">single</a></li>
                        <li><a href="single-image.html">single image</a></li>
                        <li><a href="single-slider.html">single slider</a></li>
                        <li><a href="single-youtube.html">single youtube</a></li>
                        <li><a href="single-vimeo.html">single vimeo</a></li>
                        <li><a href="single-dailymotion.html">single dailymotion</a></li>
                        <li><a href="single-soundcloud.html">single soundcloud</a></li>
                        <li><a href="single-video.html">single video</a></li>
                        <li><a href="single-audio.html">single audio</a></li>
                    </ul>
                </li>
                <li><a href="contact.html">contact</a></li>
            </ul>
        </nav>

            <!-- Aside menu -->

<!--        <div class="crt-card bg-primary text-center">
            <div class="crt-card-avatar">
                <img class="avatar avatar-195" src="<?php /*echo get_template_directory_uri().'/assets/images/uploads/avatar/avatar-195x195.png' */?>" srcset="<?php /*echo get_template_directory_uri().'/assets/images/uploads/avatar/avatar-390x390-2x.png 2x' */?>" width="195" height="195" alt="">
            </div>
            <div class="crt-card-info">
                <h2 class="text-upper">Ola Lowe</h2>

                <p class="text-muted">Florist | Decorator</p>
                <ul class="crt-social clear-list">
                    <li><a><span class="crt-icon crt-icon-facebook"></span></a></li>
                    <li><a><span class="crt-icon crt-icon-twitter"></span></a></li>
                    <li><a><span class="crt-icon crt-icon-google-plus"></span></a></li>
                    <li><a><span class="crt-icon crt-icon-instagram"></span></a></li>
                    <li><a><span class="crt-icon crt-icon-pinterest"></span></a></li>
                </ul>
            </div>
        </div>
        <aside class="widget-area">
            <section class="widget widget_search">
                <form role="search" method="get" class="search-form" action="#">
                    <label>
                        <span class="screen-reader-text">Search for:</span>
                        <input type="search" class="search-field" placeholder="Search" value="" name="s">
                    </label>
                    <button type="submit" class="search-submit">
                        <span class="screen-reader-text">Search</span>
                        <span class="crt-icon crt-icon-search"></span>
                    </button>
                </form>
            </section>

            <section class="widget widget_posts_entries">
                <h2 class="widget-title">popular posts</h2>
                <ul>
                    <li>
                        <a class="post-image" href="">
                            <img src="<?php /*echo get_template_directory_uri().'/assets/images/uploads/blog/img-70x70-01.png' */?>" alt="">
                        </a>
                        <div class="post-content">
                            <h3>
                                <a href="">contextual advertising</a>
                            </h3>
                        </div>
                        <div class="post-category-comment">
                            <a href="" class="post-category">Work</a>
                            <a href="" class="post-comments">256 comments</a>
                        </div>
                    </li>

                    <li>
                        <a class="post-image" href="">
                            <img src="<?php /*echo get_template_directory_uri().'/assets/images/uploads/blog/img-70x70-02.jpg' */?>" alt="">
                        </a>
                        <div class="post-content">
                            <h3>
                                <a href="">grilling tips for the dog days of summer</a>
                            </h3>
                        </div>
                        <div class="post-category-comment">
                            <a href="" class="post-category">Work</a>
                            <a href="" class="post-comments">256 comments</a>
                        </div>
                    </li>

                    <li>
                        <a class="post-image" href="">
                            <img src="<?php /*echo get_template_directory_uri().'/assets/images/uploads/blog/img-70x70-03.png' */?>" alt="">
                        </a>
                        <div class="post-content">
                            <h3><a href=""></a>branding do you know who are</h3>
                        </div>
                        <div class="post-category-comment">
                            <a href="" class="post-category">Work</a>
                            <a href="" class="post-comments">256 comments</a>
                        </div>
                    </li>
                </ul>
            </section>

            <section id="tag_cloud-2" class="widget widget_tag_cloud">
                <h2 class="widget-title">Tags</h2>
                <div class="tagcloud">
                    <a href="" class="tag-link-5 tag-link-position-1" title="1 topic" style="font-size: 1em;">Audios</a>
                    <a href="" class="tag-link-7 tag-link-position-2" title="1 topic" style="font-size: 1em;">Freelance</a></div>
            </section>

            <section id="recent-posts-3" class="widget widget_recent_entries">
                <h4 class="widget-title">Recent Posts</h4>
                <ul>
                    <li>
                        <a href="">Global Travel And Vacations Luxury Travel On A Tight Budget</a>
                        <div class="post-category-comment">
                            <a href="" class="post-category">Photography</a>
                            <a href="" class="post-comments">256 comments</a>
                        </div>
                    </li>
                    <li>
                        <a href="">cooking for one</a>
                        <div class="post-category-comment">
                            <a href="" class="post-category">Work</a>
                            <a href="" class="post-comments">256 comments</a>
                        </div>
                    </li>
                    <li>
                        <a href="">An Ugly Myspace Profile Will Sure Ruin Your Reputation</a>
                        <div class="post-category-comment">
                            <a href="" class="post-category">Photography</a>
                            <a href="" class="post-comments">256 comments</a>
                        </div>
                    </li>
                </ul>
            </section>

            <section class="widget widget_categories">
                <h4 class="widget-title">post categories</h4>
                <ul>
                    <li class="cat-item"><a href="">Audios</a>5</li>
                    <li class="cat-item"><a href="">Daili Inspiration</a>2</li>
                    <li class="cat-item"><a href="">Freelance</a>27</li>
                    <li class="cat-item"><a href="">Links</a>5</li>
                    <li class="cat-item"><a href="">Mobile</a>2</li>
                    <li class="cat-item"><a href="">Phography</a>27</li>
                </ul>
            </section>
        </aside>-->

    </div><!-- #crt-sidebar-inner -->
</div><!-- #crt-sidebar -->
<?php get_footer(); ?>